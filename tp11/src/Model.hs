{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Model where

import Data.Text (Text)
import Database.SQLite.Simple
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)
import GHC.Generics (Generic)

data Msg = Msg
    { _author :: Text
    , _text :: Text
    , _thread :: Text
    } deriving (Generic, Show)

instance FromRow Msg where
    fromRow = Msg <$> field <*> field <*> field

data Thread = Thread
    { _ind :: Int
    , _title :: Text
    } deriving (Generic, Show)

instance FromRow Thread where
    fromRow = Thread <$> field <*> field

dbSelectAllMsgs :: Connection -> IO [Msg]
dbSelectAllMsgs conn =
    query_ conn "SELECT msg_author, msg_text, thread_title \
                \FROM msg \
                \INNER JOIN thread ON thread_id = msg_thread"

dbSelectAllThreads :: Connection -> IO [Thread]
dbSelectAllThreads conn = query_ conn "SELECT * FROM thread"

dbSelectByIdThreads :: Connection -> Int -> IO [Thread]
dbSelectByIdThreads conn a = query_ conn "SELECT * FROM thread WHERE thread_id = \a"