{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Web.Scotty
import Lucid
import Control.Monad.IO.Class
import Database.SQLite.Simple

import View
import Model

main :: IO ()
main = scotty 3000 $ do
    get "/" $ html $ renderText indexPage
    get "/alldata" $ do
        msgs <- liftIO $ withConnection "ulcoforum.db" Model.dbSelectAllMsgs
        html $ renderText $ allDataPage msgs
    get "/allthreads" $ do
        threads <- liftIO $ withConnection "ulcoforum.db" Model.dbSelectAllThreads
        html $ renderText $ allThreadsPage threads

    get "/:id" $ do
        threads <- liftIO $ withConnection "ulcoforum.db" Model.dbSelectAllThreads
        html $ renderText $ allThreadsPage threads

        {- num <- read <$> param "id"
         thread <- liftIO $ withConnection "ulcoforum.db" Model.dbSelectByIdThreads num
    html $ renderText $ ThreadPage thread -}




