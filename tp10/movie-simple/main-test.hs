import Database.SQLite.Simple (open, close)

import Movie1
import Movie2

main :: IO ()
main = do
    conn <- open "movie.db"

    res <- Movie1.dbSelectAllMovies conn
    mapM_ print res

    res2 <- Movie2.dbSelectAllProds conn
    mapM_ print res2

    close conn

