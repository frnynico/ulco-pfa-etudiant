data User = User {nom :: String , prenom :: String, age :: Int}

showUser :: User -> String
showUser (User nom prenom age) = nom ++ " " ++ prenom ++ " " ++ show age

incAge :: User -> User
incAge (User nom prenom age) = (User nom prenom (age + 1)) 

main :: IO ()
main = do
    let user = User { nom = "Fourny", prenom = "Nicolas", age = 22 }
    print(showUser user)
    let user2 = incAge user
    print(showUser user2)

