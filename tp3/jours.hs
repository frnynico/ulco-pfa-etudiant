data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche

estWeekend :: Jour -> Bool
estWeekend j = 
    case j of
        Samedi -> True
        Dimanche -> True
        _ -> False

compterOuvrables :: [Jour] -> Int
compterOuvrables [] = 0
compterOuvrables (x:xs) | estWeekend(x) = 1 + compterOuvrables xs
                        | otherwise = compterOuvrables xs

main :: IO ()
main = putStrLn "TODO"


