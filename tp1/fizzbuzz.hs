fizzbuzz1 :: [Int] -> [String]
fizzbuzz1 [] = []
fizzbuzz1 [x] = [show x]
fizzbuzz1 (x:xs)
    | x `mod` 3 == 0 = ["Fizz"]
    | x `mod` 5 == 0 = ["Buzz"]
    | x `mod` 3 == 0 && x `mod` 5 == 0 = ["FizzBuzz"]
    | otherwise = [show x] ++ fizzbuzz1 xs

-- fizzbuzz2 :: [Int] -> [String]

-- fizzbuzz

