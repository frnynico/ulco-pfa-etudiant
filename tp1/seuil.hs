seuilInt :: Int -> Int -> Int -> Int
seuilInt x y z
    | x < y && x > z = x
    | y > x && y < z = y
    | z > x && z < y = z
    | otherwise = 0

seuilTuple :: (Int, Int, Int) -> Int
seuilTuple (x,y,z)
    | x < y && x > z = x
    | y > x && y < z = y
    | z > x && z < y = z
    | otherwise = 0

main :: IO ()
main = do
print (seuilInt 1 10 0)
print (seuilInt 1 10 2)
print (seuilInt 1 10 42)

print (seuilTuple (1,10,0))
print (seuilTuple (1,10,2))
print (seuilTuple (1,10,42))