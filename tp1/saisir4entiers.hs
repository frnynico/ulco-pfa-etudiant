import Control.Monad (forM_)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe)


main :: IO Int
main = do
    putStr "Saisie 1 : "
    entier <- getLine
    case readMaybe entier of
        Just x -> return x
        Nothing -> putStrLn "saisie invalide" >> main