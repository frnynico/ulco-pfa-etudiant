{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Site where

import Data.Aeson
import GHC.Generics
import Lucid

data Site = Site
    { imgs :: [String]
    , url  :: String
    } deriving (Generic,Show)

instance FromJSON Site

myHtml :: Html ()
myHtml = do
    doctype_
    html_ $ do
       head_ $
        body_ $ do
            h1_ "hello"
            img_ [src_ "toto.png"]
            p_ $ do
                "this is "

loadSites :: String -> IO (Either String [Site])
loadSites = eitherDecodeFileStrict
