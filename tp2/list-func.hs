mymap :: (a -> b) -> [a] -> [b]
mymap a [] = []
mymap a (x:xs) = a x : mymap a xs

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter b [] = []
myfilter b (x:xs) | b x       = x : myfilter b xs
                  | otherwise = myfilter b xs

myfoldl :: (b -> a -> b) -> b -> [a] -> b
myfoldl a b [] = b
myfoldl a b (x:xs) = foldl a (a b x) xs

-- myfoldr 

main :: IO ()
main = do
print(mymap (*2) [1..3])
print(myfilter even [1..4])
print(myfoldl (+) 2 [1..4])
