import qualified Data.Text.Encoding as E
import qualified Data.Text.IO as T
import qualified Data.ByteString.Char8 as C

main :: IO()
main = do
    --lire le texte1.hs en ByteString
    file <- C.readFile "text1.hs"
    --convertir en string
    let contents = E.decodeUtf8 file
    --afficher
    T.putStrLn contents
